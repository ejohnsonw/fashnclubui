/* 
Copyright (c) 2016 NgeosOne LLC
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Engineered using http://www.generatron.com/

[GENERATRON]
Generator :   System Templates
Filename:     CompositionAddCtrl.js
Description:  Controller to create a new record, loads foreign keys
Project:      Fashn
Template: /ionic-1.7.14/controllers/add.controller.vm
 */


'use strict';

/**
 * @ngdoc function
 * @name Fashn.controller:CompositionAddCtrl
 * @description
 * # CompositionAddCtrl
 * Controller of the Fashn
 */
angular.module('Fashn.controllers')
    .controller('CompositionAddCtrl', function($scope, UserWS, AssetWS, CompositionWS) {

        $scope.composition = {};

        $scope.edit = false;
        $scope.successcb = function(data, status, headers, config) {
            document.location.href = "#/app/compositions"
        }

        $scope.errorcb = function(data, status, headers, config) {
            //alert("Failed: "+JSON.stringify(data));
            for (var e in data.errors) {
                var error = data.errors[e];
                $("#" + error.field + "Error").html(error.message);
            }
        }

        $scope.save = function() {
            var data = {};
            data = $scope.composition;
            CompositionWS.create($scope.successcb, $scope.errorcb, data);
        }

        $scope.cancel = function() {
            document.location.href = "#/compositions"
        }

        $scope.loadReferences = function() {
            AssetWS.list(function(data) {
                $scope.assetss = data;
            }, $scope.errorcb)
            UserWS.list(function(data) {
                $scope.users = data;
            }, $scope.errorcb)

        }
        $scope.loadReferences();


    }).config(function($stateProvider) {
        $stateProvider
            .state('app.addComposition', {
                url: '/composition',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/composition.html',
                        controller: 'CompositionAddCtrl'
                    }
                }
            })

    });


/* 
[STATS]
It would take a person typing  @ 100.0 cpm, 
approximately 17.48 minutes to type the 1748+ characters in this file.
 */
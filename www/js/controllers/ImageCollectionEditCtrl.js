/* 
Copyright (c) 2016 NgeosOne LLC
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

   
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
 to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Engineered using http://www.generatron.com/

[GENERATRON]
Generator :   System Templates
Filename:     ImageCollectionEditCtrl.js
Description:  Creates a controller to edit a record, loads foreign key tables to feed dropdowns
Project:      Fashn
Template: /ionic-1.7.14/controllers/edit.controller.js.vm
 */


'use strict';

/**
 * @ngdoc function
 * @name Fashn.controller:ImageCollectionsCtrl
 * @description
 * # ImageCollectionsCtrl
 * 
 */
angular.module('Fashn.controllers')
    .controller('ImageCollectionEditCtrl', function($scope, $stateParams, $rootScope, UserWS, ImageWS, ImageCollectionWS) {
        $scope.imageCollection = {};




        $scope.edit = true;
        $scope.save = function() {
            var data = {};
            data = $scope.imageCollection;
            ImageCollectionWS.update($scope.updatesuccesscb, $scope.errorcb, data);
        }

        $scope.delete = function() {
            var data = {};
            if (BACKEND == GRAILSBACKEND) {
                data.model = $scope.imageCollection;
            } else {
                data = $scope.imageCollection;
            }
            ImageCollectionWS.delete($scope.updatesuccesscb, $scope.errorcb, data);
        }
        $scope.cancel = function() {
            document.location.href = "#/imageCollections"
        }

        $scope.updatesuccesscb = function(data, status, headers, config) {
            document.location.href = "#/imageCollections"
        }
        $scope.successcb = function(data, status, headers, config) {
            $scope.imageCollection = data;

        }

        $scope.errorcb = function(data, status, headers, config) {

            alert("Failed: " + JSON.stringify(data));
        }
        $scope.loadReferences = function() {

            // false Long String id
            // true Collection Image images
            ImageWS.list(function(data) {
                    $scope.imagess = data;
                }, $scope.errorcb)
                // false String String name
                // false User String user
            UserWS.list(function(data) {
                $scope.users = data;
            }, $scope.errorcb)

        }
        $scope.loadReferences();
        ImageCollectionWS.retrieve($scope.successcb, $scope.errorcb, $stateParams.id);
    }).config(function($stateProvider) {
        $stateProvider
            .state('app.editImageCollection', {
                url: '/imageCollection/:id',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/imageCollection.html',
                        controller: 'ImageCollectionEditCtrl'
                    }
                }
            })
    });


/* 
[STATS]
It would take a person typing  @ 100.0 cpm, 
approximately 22.22 minutes to type the 2222+ characters in this file.
 */